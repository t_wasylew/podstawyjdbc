package pl.sda.web;


import pl.sda.dto.Worker;
import pl.sda.pl.sda.dao.WorkersDAO;
import pl.sda.pl.sda.dao.WorkersDAOHibernateImpl;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import java.util.List;

@SessionScoped
@ManagedBean(name = "workerList")
public class WorkersListManagedBean {

    private WorkersDAO workersDAO = new WorkersDAOHibernateImpl();
    private Worker newWorker = new Worker();

    private String filter;

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    @PostConstruct

    public List<Worker> getList() {
        //TODO: wczytać pracowników z bazy
        return workersDAO.getAllWorkers();
        //getFilterWorkers
    }

    public void refresh() {}


    public void addNewWorker() {
        workersDAO.saveWorker(newWorker);
        newWorker = new Worker();
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Nowy pracownik został dodany!"));
    }

    public void deleteWorker(Integer id) {
        workersDAO.deleteWorker(id);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Usunięcie pracownika!"));
    }

    public void editWorker(Integer id) {
        newWorker = workersDAO.getWorker(id);

        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Teraz mozesz edytowac dane!"));
    }

    public void saveWorker() {
        workersDAO.updateWorker(newWorker);
        newWorker = new Worker();

        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Zmieniono dane!"));
    }

    public Worker getNewWorker() {
        return newWorker;
    }
}
