package pl.sda.pl.sda.dao;

import pl.sda.dto.Worker;
import java.util.List;

public interface WorkersDAO {



     List<Worker> getAllWorkers();

//    List<Worker> getWorkersByFilter(String filter);

     void saveWorker(Worker newWorker);

     void deleteWorker(Integer id);

     void updateWorker(Worker worker);

     Worker getWorker(Integer id);

}
