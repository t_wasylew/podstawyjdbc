package pl.sda.pl.sda.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import pl.sda.dto.Worker;

import java.util.List;

public class WorkersDAOHibernateImpl implements WorkersDAO{

    private SessionFactory sessionFactory;
    public WorkersDAOHibernateImpl() {
    sessionFactory= new Configuration().configure().buildSessionFactory();
    }

    @Override
    public List<Worker> getAllWorkers() {
        Session session = sessionFactory.openSession();

        List<Worker>  workers=  session.createQuery("from Worker").list();
//        List<Worker> workers = session.createCriteria(Worker.class).list();
        session.close();
        return workers;
    }

    @Override
    public void saveWorker(Worker newWorker) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.save(newWorker);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public void deleteWorker(Integer id) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.delete(getWorker(id));
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public void updateWorker(Worker worker) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.update(worker);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public Worker getWorker(Integer id) {
        Session session = sessionFactory.openSession();
        Worker worker = session.get(Worker.class, id);
        return worker;
    }
}
