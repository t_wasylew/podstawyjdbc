package pl.sda.pl.sda.dao;

import pl.sda.dto.Worker;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class WorkersDAOJDBCImpl implements WorkersDAO{


    private Connection getConnection() throws SQLException {
        Connection connection;
        connection = DriverManager.getConnection("jdbc:mysql://localhost/worker_app", "root", "w583358583");
        return connection;
    }

    public List<Worker> getAllWorkers() {
        List<Worker> workerList = new ArrayList<>();
        Connection connection = null;
        try {
            connection = getConnection();
            PreparedStatement statement = connection.prepareStatement("select*from workers");
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {

                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                String position = resultSet.getString("position");
                int salary = resultSet.getInt("salary");
                int birthYear = resultSet.getInt("birthYear");
                Integer id = resultSet.getInt("id");

                workerList.add(new Worker(firstName, lastName, position, salary, birthYear, id));

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return workerList;
    }

    public void saveWorker(Worker newWorker) {
        Connection connection = null;
        try {
            connection = getConnection();
            PreparedStatement statement = connection.prepareStatement("insert into workers " +
                    "(first_name, last_name, position, salary, birthYear) values (?,?,?,?,?)");
            statement.setString(1, newWorker.getFirstName());
            statement.setString(2, newWorker.getLastName());
            statement.setString(3, newWorker.getPosition());
            statement.setInt(4, newWorker.getSalary());
            statement.setInt(5, newWorker.getBirthYear());
            statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public void deleteWorker(Integer id) {
        Connection connection = null;
        try {
            connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("delete from workers where workers.id='" + id + "'");
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public void updateWorker(Worker worker) {
        Connection connection = null;
        try {
            connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("UPDATE workers SET first_name= ?, " +
                    "last_name= ?, position= ?, salary= ?, birthYear= ? where id = ?");
            preparedStatement.setString(1,worker.getFirstName() );
            preparedStatement.setString(2, worker.getLastName());
            preparedStatement.setString(3, worker.getPosition());
            preparedStatement.setInt(4, worker.getSalary());
            preparedStatement.setInt(5, worker.getBirthYear());
            preparedStatement.setInt(6, worker.getId());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public Worker getWorker(Integer id) {
        Connection connection = null;
        Worker worker;
        try {
            connection = getConnection();
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM workers where id=?");
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                String firstName = resultSet.getString("first_name");
                String lastName= resultSet.getString("last_name");
                String position = resultSet.getString("position");
                int salary = resultSet.getInt("salary");
                int birthYear= resultSet.getInt("birthYear");
                worker = new Worker(firstName, lastName, position, salary, birthYear, id);
                return worker;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return null;
    }
}
